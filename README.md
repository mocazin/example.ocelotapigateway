# Product Name
> Short blurb about what your product does.

One to two paragraph statement about your product and what it does.

![](GeneralFlow.png)

## Software Version

* .NET Core
* Angular ^8.0.0 (LTS)

## Setup

**Angular & Npm Install**

[Download node](https://nodejs.org/en/download/)
```csharp
npm install -g @angular/cli
ng new my-dream-app
cd my-dream-app
ng serve
```

## Dev Tools

* Visual Studio Professional  (2017 or 2019)
* VS Code (suited for Angular)
* PostgreSQL 10 or Later version

## Servers

| Environment   | Role          |
| ------------- | ------------- |
| Content Cell  | Content Cell  |
| Content Cell  | Content Cell  |

## Contributing

1. Create your feature branch (`git checkout -b feature/jira-no#`)
2. Commit your changes (`git commit -am 'Something changed'`)
3. Push to the branch (`git push origin feature/jira-no#`)
4. Create a new Pull Request