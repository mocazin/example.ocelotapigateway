﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductsApi
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(GetProducts());
        }

        private IList<Product> GetProducts() 
        {

            return new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Amout = 500.00m,
                    Name = "HP Pro"
                },
                new Product
                {
                    Id = 2,
                    Amout = 550.99m,
                    Name = "MacBook Pro"
                }

            };
        }

        class Product
        {
            public int Id { get; set; }
            public decimal Amout { get; set; }
            public string Name { get; set; } 
        }
    }
}
