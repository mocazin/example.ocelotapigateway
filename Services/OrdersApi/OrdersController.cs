﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OrdersApi
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(GetOrders());
        }

        private IList<Order> GetOrders()
        {

            return new List<Order>
            {
                new Order
                {
                    Id = 1,
                    Amout = 500.00m
                },
                new Order
                {
                    Id = 2,
                    Amout = 550.99m
                }

            };
        }

        class Order
        {
            public int Id { get; set; }
            public decimal Amout { get; set; }
        }
    }
}
